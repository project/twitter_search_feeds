<?php
?>
<table class="node2<?php if ($teaser) { if($sticky) { print " sticky"; } else { print " teaser"; } } ?><?php if (!$status) print " node2-unpublished"; ?>"> 
  <?php 
    if($teaser) {
      if($sticky) $prefix = 'sticky-'; 
      else $prefix = 'teaser-';
    }
    else {
      $prefix = '';
    }
  ?>

  <tr>
  <td class="<?php print $prefix; ?>node2-tl"></td>
  <td class="<?php print $prefix; ?>node2-tc"></td>
  <td class="<?php print $prefix; ?>node2-tr"></td>
  </tr>

  <tr>
  <td class="<?php print $prefix; ?>node2-ml"></td>
  <td class="<?php print $prefix; ?>node2-mc">

    <table class="node2title">
    <tr>
      <td>
        <h2 class="title"><a href="<?php print $node_url; ?>"><?php print $title?></a></h2>
        <span class="submitted"><?php print $submitted?></span>
        <span class="taxonomy"><?php print $terms?></span>
      </td>
    </tr>
    </table>
    <?php if($picture) print $picture; ?>
    <div class="content"><?php print $content?></div>
    <?php if ($links) { ?><div class="links"><?php print $links?></div><?php }; ?>

  </td>
  <td class="<?php print $prefix; ?>node2-mr"></td>
  </tr>

  <tr>
  <td class="<?php print $prefix; ?>node2-bl"></td>
  <td class="<?php print $prefix; ?>node2-bc"></td>
  <td class="<?php print $prefix; ?>node2-br"></td>
  </tr>
</table>
